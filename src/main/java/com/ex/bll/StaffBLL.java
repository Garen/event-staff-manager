package com.ex.bll;

import com.ex.dal.DALException;
import com.ex.dal.StaffDAL;
import com.ex.model.StaffModel;
import java.util.List;

public class StaffBLL {

    private StaffDAL dal;

    public StaffBLL(StaffDAL dal){
        this.dal = dal;
    }

    public int createStaff(StaffModel model) throws DALException {
        return dal.createStaff(model);
    }

    public List<StaffModel> getAllStaff() throws DALException{
        return dal.getAllStaff();
    }
    public StaffModel getStaffByID(int id) throws DALException{
        return dal.getStaffByID(id);
    }

    public void updateStaffByID(StaffModel model) throws DALException{ dal.updateStaffByID(model); }

    public void deleteStaffByID(int id) throws DALException{ dal.deleteStaffByID(id); }
}
