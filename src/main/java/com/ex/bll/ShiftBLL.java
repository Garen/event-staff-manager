package com.ex.bll;

import com.ex.dal.DALException;
import com.ex.dal.ShiftDAL;
import com.ex.model.ShiftModel;

import java.util.List;

public class ShiftBLL {

    private ShiftDAL dal;

    public ShiftBLL(ShiftDAL dal){
        this.dal = dal;
    }

    public int createShift(ShiftModel model) throws DALException{
        return dal.createShift(model);
    }

    public List<ShiftModel> getAllShifts() throws DALException{
        return dal.getAllShifts();
    }
    public ShiftModel getShiftByID(int id) throws DALException{
        return dal.getShiftByID(id);
    }

    public void updateShiftByID(ShiftModel model) throws DALException{ dal.updateShiftByID(model); }

    public void deleteShiftByID(int id) throws DALException{ dal.deleteShiftByID(id); }
}
