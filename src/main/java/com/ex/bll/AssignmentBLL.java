package com.ex.bll;

import com.ex.dal.AssignmentDAL;
import com.ex.dal.DALException;
import com.ex.model.AssignmentModel;

import java.util.List;

public class AssignmentBLL {

    private AssignmentDAL dal;

    public AssignmentBLL(AssignmentDAL dal) {
        this.dal = dal;
    }

    public void createAssignment(AssignmentModel model) throws DALException{
        dal.createAssignment(model);
    }

    public List<AssignmentModel> getAllAssignments() throws DALException{
        return dal.getAllAssignments();
    }
    public List<AssignmentModel> getAssignmentsByStaffID(int id) throws DALException{
        return dal.getAssignmentsByStaffID(id);
    }
    public List<AssignmentModel> getAssignmentsByShiftID(int id)  throws DALException{
        return dal.getAssignmentsByShiftID(id);
    }

    public void deleteAssignmentByID(int staffID, int shiftID)  throws DALException{
        dal.deleteAssignmentByID(staffID, shiftID);
    }
}
