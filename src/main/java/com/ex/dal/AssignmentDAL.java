package com.ex.dal;

import com.ex.model.AssignmentModel;

import java.io.Closeable;
import java.util.List;

public abstract class AssignmentDAL implements AutoCloseable{

    public abstract void createAssignment(AssignmentModel model) throws DALException;

    public abstract List<AssignmentModel> getAllAssignments() throws DALException;
    public abstract List<AssignmentModel> getAssignmentsByStaffID(int id) throws DALException;
    public abstract List<AssignmentModel> getAssignmentsByShiftID(int id) throws DALException;

    public abstract void deleteAssignmentByID(int staffID, int shiftID) throws DALException;
}
