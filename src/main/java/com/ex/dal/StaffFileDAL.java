package com.ex.dal;

import com.ex.model.ShiftModel;
import com.ex.model.StaffModel;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Deprecated
public class StaffFileDAL extends StaffDAL{

    private String file;
    private BufferedWriter writer;

    public StaffFileDAL(String file) throws DALException{
        this.file = file;
        try {
            writer = new BufferedWriter(new FileWriter(file));
        }
        catch(IOException e){
            throw new DALException(e);
        }
    }

    @Override
    public void close() throws IOException {
        writer.close();
    }

    @Override
    public int createStaff(StaffModel model) throws DALException{
        try {
            if(model.id == 0)
                //Yeah this probably makes it so that its not guaranteed to be unique buuuut
                //We generate IDs like this because we don't have access to SQL auto increment
                model = new StaffModel(Math.abs((int) UUID.randomUUID().getMostSignificantBits()), model.firstName, model.lastName, model.phoneNumber, model.email);
            writer.append(model.toFileString());
            writer.flush();
            return model.id;
        }
        catch (IOException e){
            throw new DALException(e);
        }
    }

    @Override
    public List<StaffModel> getAllStaff() throws DALException{
        throw new NotImplementedException();
    }

    @Override
    public StaffModel getStaffByID(int id) throws DALException{
        throw new NotImplementedException();
    }

    @Override
    public void updateStaffByID(StaffModel model) throws DALException{
        throw new NotImplementedException();
    }

    @Override
    public void deleteStaffByID(int id) throws DALException{
        throw new NotImplementedException();
    }
}
