package com.ex.dal;

import com.ex.model.AssignmentModel;
import com.ex.model.StaffModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class AssignmentSQLDAL extends AssignmentDAL{

    private Connection con;

    public AssignmentSQLDAL(String url, String username, String password) throws DALException {
        try {
            con = DBUtil.getInstance().getConnection(url, username, password);
        }
        catch (SQLException e){
            throw new DALException(e);
        }
    }

    @Override
    public void createAssignment(AssignmentModel model) throws DALException {
        try {
            String sql = "INSERT INTO public.assignment(staffID, shiftID) VALUES (?, ?);";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, model.staffID);
            ps.setInt(2, model.shiftID);
            ps.execute();
        }
        catch (SQLException e){
            throw new DALException(e);
        }
    }

    @Override
    public List<AssignmentModel> getAllAssignments() throws DALException {
        try{
            String sql = "SELECT * FROM public.assignment;";
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<AssignmentModel> assignmentList = new LinkedList<AssignmentModel>();

            while(rs.next()){
                int staffID = rs.getInt("staffID");
                int shiftID = rs.getInt("shiftID");

                assignmentList.add(new AssignmentModel(staffID, shiftID));
            }

            return assignmentList;
        }
        catch(SQLException e){
            throw new DALException(e);
        }
    }

    @Override
    public List<AssignmentModel> getAssignmentsByStaffID(int id) throws DALException {
        try{
            String sql = "SELECT * FROM public.assignment WHERE staffID = ?;";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            List<AssignmentModel> assignmentList = new LinkedList<AssignmentModel>();

            while(rs.next()){
                int staffID = rs.getInt("staffID");
                int shiftID = rs.getInt("shiftID");

                assignmentList.add(new AssignmentModel(staffID, shiftID));
            }

            return assignmentList;
        }
        catch(SQLException e){
            throw new DALException(e);
        }
    }

    @Override
    public List<AssignmentModel> getAssignmentsByShiftID(int id) throws DALException {
        try{
            String sql = "SELECT * FROM public.assignment WHERE shiftID = ?;";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            List<AssignmentModel> assignmentList = new LinkedList<AssignmentModel>();

            while(rs.next()){
                int staffID = rs.getInt("staffID");
                int shiftID = rs.getInt("shiftID");

                assignmentList.add(new AssignmentModel(staffID, shiftID));
            }

            return assignmentList;
        }
        catch(SQLException e){
            throw new DALException(e);
        }
    }

    @Override
    public void deleteAssignmentByID(int staffID, int shiftID) throws DALException {
        try{
            String sql = "DELETE FROM public.assignment WHERE staffID = ? AND shiftID = ?;";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, staffID);
            ps.setInt(2, shiftID);
            ps.execute();
        }
        catch(SQLException e){
            throw new DALException(e);
        }
    }

    @Override
    public void close() throws Exception {
        con.close();
    }
}
