package com.ex.dal;

import com.ex.model.ShiftModel;

import java.util.List;

public abstract class ShiftDAL implements AutoCloseable{

    /***
     * @param model
     * @return the id of the created model
     * @throws DALException
     */
    public abstract int createShift(ShiftModel model) throws DALException;

    public abstract List<ShiftModel> getAllShifts() throws DALException;
    public abstract ShiftModel getShiftByID(int id) throws DALException;

    public abstract void updateShiftByID(ShiftModel model) throws DALException;

    public abstract void deleteShiftByID(int id) throws DALException;
}
