package com.ex.dal;

import com.ex.model.StaffModel;

import java.util.List;

public abstract class StaffDAL implements AutoCloseable{

    /***
     * @param model
     * @return the id of the created model
     * @throws DALException
     */
    public abstract int createStaff(StaffModel model) throws DALException;

    public abstract List<StaffModel> getAllStaff() throws DALException;
    public abstract StaffModel getStaffByID(int id) throws DALException;

    public abstract void updateStaffByID(StaffModel model) throws DALException;

    public abstract void deleteStaffByID(int id) throws DALException;
}
