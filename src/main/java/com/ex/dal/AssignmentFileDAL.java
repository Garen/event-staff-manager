package com.ex.dal;

import com.ex.model.AssignmentModel;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

@Deprecated
public class AssignmentFileDAL extends AssignmentDAL{

    private String file;
    private BufferedWriter writer;

    public AssignmentFileDAL(String file) throws DALException{
        this.file = file;
        try {
            writer = new BufferedWriter(new FileWriter(file));
        }
        catch(IOException e){
            throw new DALException(e);
        }
    }

    @Override
    public void close() throws IOException{
        writer.close();
    }

    @Override
    public void createAssignment(AssignmentModel model) throws DALException{
        try {
            writer.append(model.toFileString());
            writer.flush();
        }
        catch (IOException e){
            throw new DALException(e);
        }
    }

    @Override
    public List<AssignmentModel> getAllAssignments() throws DALException{
        throw new NotImplementedException();
    }

    @Override
    public List<AssignmentModel> getAssignmentsByStaffID(int id) throws DALException{
        throw new NotImplementedException();
    }

    @Override
    public List<AssignmentModel> getAssignmentsByShiftID(int id) throws DALException{
        throw new NotImplementedException();
    }

    @Override
    public void deleteAssignmentByID(int staffID, int shiftID) throws DALException{
        throw new NotImplementedException();
    }
}
