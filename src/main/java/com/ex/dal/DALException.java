package com.ex.dal;

public class DALException extends Exception{

    //constructors arent inherited so im just writing the basic exception constructors

    DALException(){
        super();
    }

    DALException(String message){
        super(message);
    }

    DALException(String message, Throwable cause){
        super(message, cause);
    }

    DALException(Throwable cause){
        super(cause);
    }
}
