package com.ex.dal;

import com.ex.model.ShiftModel;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Deprecated
public class ShiftFileDAL extends ShiftDAL {

    private String file;
    private BufferedWriter writer;

    public ShiftFileDAL(String file) throws DALException{
        this.file = file;
        try {
            writer = new BufferedWriter(new FileWriter(file));
        }
        catch(IOException e){
            throw new DALException(e);
        }
    }

    @Override
    public void close() throws IOException {
        writer.close();
    }

    @Override
    public int createShift(ShiftModel model) throws DALException{
        try {
            if(model.id == 0)
                //Yeah this probably makes it so that its not guaranteed to be unique buuuut
                //We generate IDs like this because we don't have access to SQL auto increment
                model = new ShiftModel(Math.abs((int) UUID.randomUUID().getMostSignificantBits()), model.name, model.clientName, model.clientPhoneNumber, model.address, model.date, model.wage);
            writer.append(model.toFileString());
            writer.flush();
            return model.id;
        }
        catch (IOException e){
            throw new DALException(e);
        }
    }

    @Override
    public List<ShiftModel> getAllShifts() throws DALException{
        throw new NotImplementedException();
    }

    @Override
    public ShiftModel getShiftByID(int id) throws DALException{
        throw new NotImplementedException();
    }

    @Override
    public void updateShiftByID(ShiftModel model) throws DALException{
        throw new NotImplementedException();
    }

    @Override
    public void deleteShiftByID(int id) throws DALException{
        throw new NotImplementedException();
    }
}
