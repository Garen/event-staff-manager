package com.ex.dal;

import com.ex.model.StaffModel;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class StaffSQLDAL extends StaffDAL{

    private Connection con;

    public StaffSQLDAL(String url, String username, String password) throws DALException {
        try {
            con = DBUtil.getInstance().getConnection(url, username, password);
        }
        catch (SQLException e){
            throw new DALException(e);
        }
    }

    @Override
    public int createStaff(StaffModel model) throws DALException {
        try {
            String sql = "INSERT INTO public.staff(firstName, lastName, phoneNumber, email) VALUES (?, ?, ?, ?) RETURNING id;";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, model.firstName);
            ps.setString(2, model.lastName);
            ps.setString(3, model.phoneNumber);
            ps.setString(4, model.email);
            ResultSet rs = ps.executeQuery();
            if(rs.next())
                return rs.getInt("id");
            else //should never happen
                throw new DALException(new SQLException("ID was not returned during row creation"));
        }
        catch (SQLException e){
            throw new DALException(e);
        }
    }

    @Override
    public List<StaffModel> getAllStaff() throws DALException {
        try{
            String sql = "SELECT * FROM public.staff;";
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<StaffModel> staffList = new LinkedList<StaffModel>();

            while(rs.next()){
                int tmpId = rs.getInt("id");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String phoneNumber = rs.getString("phoneNumber");
                String email = rs.getString("email");

                staffList.add(new StaffModel(tmpId, firstName, lastName, phoneNumber, email));
            }

            return staffList;
        }
        catch(SQLException e){
            throw new DALException(e);
        }
    }

    @Override
    public StaffModel getStaffByID(int id) throws DALException {
        try{
            String sql = "SELECT * FROM public.staff WHERE id = ?;";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                int tmpId = rs.getInt("id");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String phoneNumber = rs.getString("phoneNumber");
                String email = rs.getString("email");

                return new StaffModel(tmpId, firstName, lastName, phoneNumber, email);
            }

            return null;
        }
        catch(SQLException e){
            throw new DALException(e);
        }
    }

    /***
     * Updates a row in the staff table
     * @param model The id will be the id to update and other values are applied to the database
     * @throws DALException
     */
    @Override
    public void updateStaffByID(StaffModel model) throws DALException {
        try{
            String sql = "UPDATE public.staff SET firstName = ?, lastName = ?, phoneNumber = ?, email = ? WHERE id = ?;";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, model.firstName);
            ps.setString(2, model.lastName);
            ps.setString(3, model.phoneNumber);
            ps.setString(4, model.email);
            ps.setInt(5, model.id);
            ps.execute();
        }
        catch(SQLException e){
            throw new DALException(e);
        }
    }

    @Override
    public void deleteStaffByID(int id) throws DALException {
        try{
            String sql = "DELETE FROM public.staff WHERE id = ?;";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ps.execute();
        }
        catch(SQLException e){
            throw new DALException(e);
        }
    }

    @Override
    public void close() throws Exception {
        con.close();
    }
}
