package com.ex.dal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.postgresql.Driver;


/***
 * A singleton class to ensure we only make one driver
 */
public class DBUtil{

    private static DBUtil instance;

    static DBUtil getInstance() throws SQLException{
        if(instance == null){
            instance = new DBUtil();
        }
        return instance;
    }

    private DBUtil() throws SQLException{
        DriverManager.registerDriver(new Driver());
    }

    Connection getConnection(String url, String username, String password) throws SQLException {
        return DriverManager.getConnection(url, username, password);
    }
}
