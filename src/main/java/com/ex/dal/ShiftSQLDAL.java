package com.ex.dal;

import com.ex.model.ShiftModel;
import com.ex.model.StaffModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class ShiftSQLDAL extends ShiftDAL{

    private Connection con;

    public ShiftSQLDAL(String url, String username, String password) throws DALException {
        try {
            con = DBUtil.getInstance().getConnection(url, username, password);
        }
        catch (SQLException e){
            throw new DALException(e);
        }
    }

    @Override
    public int createShift(ShiftModel model) throws DALException {
        try {
            String sql = "INSERT INTO public.shift(name, clientName, clientPhoneNumber, address, date, wage) VALUES (?, ?, ?, ?, ?, ?) RETURNING id;";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, model.name);
            ps.setString(2, model.clientName);
            ps.setString(3, model.clientPhoneNumber);
            ps.setString(4, model.address);
            ps.setString(5, model.date);
            ps.setDouble(6, model.wage);
            ResultSet rs = ps.executeQuery();
            if(rs.next())
                return rs.getInt("id");
            else //should never happen
                throw new DALException(new SQLException("ID was not returned during row creation"));
        }
        catch (SQLException e){
            throw new DALException(e);
        }
    }

    @Override
    public List<ShiftModel> getAllShifts() throws DALException {
        try{
            String sql = "SELECT * FROM public.shift;";
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<ShiftModel> shiftList = new LinkedList<ShiftModel>();

            while(rs.next()){
                int tmpId = rs.getInt("id");
                String name = rs.getString("name");
                String clientName = rs.getString("clientName");
                String clientPhoneNumber = rs.getString("clientPhoneNumber");
                String address = rs.getString("address");
                String date = rs.getString("date");
                Double wage = rs.getDouble("wage");

                shiftList.add(new ShiftModel(tmpId, name, clientName, clientPhoneNumber, address, date, wage));
            }

            return shiftList;
        }
        catch(SQLException e){
            throw new DALException(e);
        }
    }

    @Override
    public ShiftModel getShiftByID(int id) throws DALException {
        try{
            String sql = "SELECT * FROM public.shift WHERE id = ?;";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                int tmpId = rs.getInt("id");
                String name = rs.getString("name");
                String clientName = rs.getString("clientName");
                String clientPhoneNumber = rs.getString("clientPhoneNumber");
                String address = rs.getString("address");
                String date = rs.getString("date");
                Double wage = rs.getDouble("wage");

                return new ShiftModel(tmpId, name, clientName, clientPhoneNumber, address, date, wage);
            }

            return null;
        }
        catch(SQLException e){
            throw new DALException(e);
        }
    }

    @Override
    public void updateShiftByID(ShiftModel model) throws DALException {
        try {
            String sql = "UPDATE public.shift SET name = ?, clientName = ?, clientPhoneNumber = ?, address = ?, date = ?, wage = ? WHERE id = ?;";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, model.name);
            ps.setString(2, model.clientName);
            ps.setString(3, model.clientPhoneNumber);
            ps.setString(4, model.address);
            ps.setString(5, model.date);
            ps.setDouble(6, model.wage);
            ps.setInt(7, model.id);
            ps.execute();
        }
        catch (SQLException e){
            throw new DALException(e);
        }
    }

    @Override
    public void deleteShiftByID(int id) throws DALException {
        try{
            String sql = "DELETE FROM public.shift WHERE id = ?;";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ps.execute();
        }
        catch(SQLException e){
            throw new DALException(e);
        }
    }

    @Override
    public void close() throws Exception {
        con.close();
    }
}
