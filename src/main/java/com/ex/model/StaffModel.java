package com.ex.model;

public class StaffModel {

    //region Values
    /***
     * Primary key, foreign key in assignment table
     * @see AssignmentModel
     */
    public final int id;
    public final String firstName;
    public final String lastName;
    public final String phoneNumber;
    public final String email;
    //endregion

    /***
     * All the values are passed in here, there is no 'no parameter'
     * constructor because all the values are required
     * @param id This will be the primary key
     * @param firstName
     * @param lastName
     * @param phoneNumber
     * @param email
     */
    public StaffModel(int id, String firstName, String lastName, String phoneNumber, String email){
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }


    /***
     * Calls {@link #StaffModel(int, String, String, String, String)}, passing 0 as the ID
     * @param firstName
     * @param lastName
     * @param phoneNumber
     * @param email
     */
    public StaffModel(String firstName, String lastName, String phoneNumber, String email){
        this(0, firstName, lastName, phoneNumber, email);
    }

    @Override
    public String toString() {
        return "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", email='" + email + '\'';
    }

    public String toFileString() {
        return id + ' ' + firstName + ' ' + lastName + ' ' + phoneNumber + ' ' + email;
    }
}
