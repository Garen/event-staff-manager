package com.ex.model;

import java.util.Date;

public class ShiftModel {

    //region Values
    /***
     * Primary key, foreign key in assignment table
     * @see AssignmentModel
     */
    public final int id;
    public final String name;
    public final String clientName;
    public final String clientPhoneNumber;
    public final String address;
    public final String date;
    public final double wage;
    //endregion

    /***
     * All the values are passed in here, there is no 'no parameter'
     * constructor because all the values are required
     * @param id This will be the primary key
     * @param name
     * @param clientName
     * @param clientPhoneNumber
     * @param address
     * @param date
     * @param wage
     */
    public ShiftModel(int id, String name, String clientName, String clientPhoneNumber, String address, String date, double wage){
        this.id = id;
        this.name = name;
        this.clientName = clientName;
        this.clientPhoneNumber = clientPhoneNumber;
        this.address = address;
        this.date = date;
        this.wage = wage;
    }

    /***
     * Calls {@link #ShiftModel(int, String, String, String, String, String, double)}, passing 0 for the id
     * @param name
     * @param clientName
     * @param clientPhoneNumber
     * @param address
     * @param date
     * @param wage
     */
    public ShiftModel(String name, String clientName, String clientPhoneNumber, String address, String date, double wage){
        this(0, name, clientName, clientPhoneNumber, address, date, wage);
    }

    @Override
    public String toString() {
        return "id=" + id +
                ", name='" + name + '\'' +
                ", clientName='" + clientName + '\'' +
                ", clientPhoneNumber='" + clientPhoneNumber + '\'' +
                ", address='" + address + '\'' +
                ", date='" + date + '\'' +
                ", wage=" + wage;
    }

    public String toFileString() {
        return id + ' ' + name + ' ' + clientName + ' ' + clientPhoneNumber + ' ' + address + ' ' + date + ' ' + wage;
    }
}
