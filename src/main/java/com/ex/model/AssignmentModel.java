package com.ex.model;

import java.util.StringJoiner;

public class AssignmentModel {

    //region Values
    /***
     * Foreign key referencing staff table
     * @see StaffModel
     */
    public final int staffID;
    /***
     * Foreign key referencing shift table
     * @see ShiftModel
     */
    public final int shiftID;
    //endregion

    /***
     * All the values are passed in here, there is no 'no parameter'
     * constructor because all the values are required
     * note that the primary keys are (staffID, shiftID)
     * @param staffID Foreign key referencing the staff table
     * @param shiftID Foreign key referencing the shift table
     */
    public AssignmentModel(int staffID, int shiftID){
        this.staffID = staffID;
        this.shiftID = shiftID;
    }

    @Override
    public String toString() {
        return "staffID=" + staffID +
                ", shiftID=" + shiftID;
    }

    public String toFileString() {
        return staffID + " " + shiftID;
    }
}
