package com.ex.app;

import com.ex.bll.*;
import com.ex.dal.*;
import com.ex.view.*;

public class ConsoleSQLApplication extends Application{

    private final static String URL = "jdbc:postgresql://event-staff-manager.cnjiicotuaxa.us-east-1.rds.amazonaws.com:5432/postgres";
    private final static String USERNAME = "trevor_serpas";
    private final static String PASSWORD = System.getenv("DB_PASSWORD");

    @Override
    public void run() {
        //the try-with-resources block will call close() in the 'finally' block automatically
        try(StaffDAL staffDAL = new StaffSQLDAL(URL, USERNAME, PASSWORD);
            ShiftDAL shiftDAL = new ShiftSQLDAL(URL, USERNAME, PASSWORD);
            AssignmentDAL assignmentDAL = new AssignmentSQLDAL(URL, USERNAME, PASSWORD))
        {
            View v = new ConsoleView(new StaffBLL(staffDAL), new ShiftBLL(shiftDAL), new AssignmentBLL(assignmentDAL));
            v.run();
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("DAL's could not initialize properly");
        }
    }
}
