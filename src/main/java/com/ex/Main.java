package com.ex;

import com.ex.app.*;

public class Main {
    public static void main(String[] args) {
        Application app = new ConsoleSQLApplication();
        app.run();
    }
}
