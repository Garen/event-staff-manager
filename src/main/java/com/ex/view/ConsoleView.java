package com.ex.view;

import com.ex.bll.AssignmentBLL;
import com.ex.bll.ShiftBLL;
import com.ex.bll.StaffBLL;
import com.ex.dal.DALException;
import com.ex.model.AssignmentModel;
import com.ex.model.ShiftModel;
import com.ex.model.StaffModel;

import java.util.Scanner;

public class ConsoleView extends View{

    private StaffBLL staffBLL;
    private ShiftBLL shiftBLL;
    private AssignmentBLL assignmentBLL;

    private Scanner in;

    public ConsoleView(StaffBLL staffBLL, ShiftBLL shiftBLL, AssignmentBLL assignmentBLL){
        this.staffBLL = staffBLL;
        this.shiftBLL = shiftBLL;
        this.assignmentBLL = assignmentBLL;

        in = new Scanner(System.in);
    }

    @Override
    public void run(){

        System.out.println("Welcome to the event staff management app!");
        int input = 0;
        int tmpID;

        do{
            input = showMenuAndGetChoice();

            switch (input){
                //region Create
                case 1:
                    try {
                        staffBLL.createStaff(getStaffModelFromInput());
                    }
                    catch(DALException e){
                        e.printStackTrace();
                        System.out.println("Staff creation failed");
                    }
                    break;
                case 2:
                    try {
                        shiftBLL.createShift(getShiftModelFromInput());
                    }
                    catch(DALException e){
                        e.printStackTrace();
                        System.out.println("Shift creation failed");
                    }
                    break;
                case 3:
                    try {
                        assignmentBLL.createAssignment(getAssignmentModelFromInput());
                    }
                    catch(DALException e){
                        e.printStackTrace();
                        System.out.println("Assignment creation failed");
                    }
                    break;
                //endregion

                //region Read
                case 4:
                    System.out.println("Displaying all staff");
                    try {
                        for (StaffModel staff : staffBLL.getAllStaff()) {
                            System.out.println(staff);
                        }
                    }
                    catch (DALException e){
                        e.printStackTrace();
                        System.out.println("Read failed");
                    }
                    System.out.println("Press enter to continue.");
                    in.nextLine();
                    break;
                case 5:
                    System.out.println("Displaying all shifts");
                    try {
                        for (ShiftModel shift : shiftBLL.getAllShifts()) {
                            System.out.println(shift);
                        }
                    }
                    catch (DALException e){
                        e.printStackTrace();
                        System.out.println("Read failed");
                    }
                    System.out.println("Press enter to continue.");
                    in.nextLine();
                    break;
                case 6:
                    System.out.println("Displaying all assignments");
                    try {
                        for (AssignmentModel assignment : assignmentBLL.getAllAssignments()) {
                            System.out.println(assignment);
                        }
                    }
                    catch (DALException e){
                        e.printStackTrace();
                        System.out.println("Read failed");
                    }
                    System.out.println("Press enter to continue.");
                    in.nextLine();
                    break;
                case 7:
                    System.out.print("Please input an id: ");
                    tmpID = in.nextInt();
                    in.nextLine();
                    try{
                        System.out.println(staffBLL.getStaffByID(tmpID));
                    }
                    catch (DALException e){
                        e.printStackTrace();
                        System.out.println("Read failed");
                    }
                    System.out.println("Press enter to continue.");
                    in.nextLine();
                    break;
                case 8:
                    System.out.print("Please input an id: ");
                    tmpID = in.nextInt();
                    in.nextLine();
                    try{
                        System.out.println(shiftBLL.getShiftByID(tmpID));
                    }
                    catch (DALException e){
                        e.printStackTrace();
                        System.out.println("Read failed");
                    }
                    System.out.println("Press enter to continue.");
                    in.nextLine();
                    break;
                case 9:
                    System.out.print("Please input an id: ");
                    tmpID = in.nextInt();
                    in.nextLine();
                    try{
                        System.out.println(assignmentBLL.getAssignmentsByStaffID(tmpID));
                    }
                    catch (DALException e){
                        e.printStackTrace();
                        System.out.println("Read failed");
                    }
                    System.out.println("Press enter to continue.");
                    in.nextLine();
                    break;
                case 10:
                    System.out.print("Please input an id: ");
                    tmpID = in.nextInt();
                    in.nextLine();
                    try{
                        System.out.println(assignmentBLL.getAssignmentsByShiftID(tmpID));
                    }
                    catch (DALException e){
                        e.printStackTrace();
                        System.out.println("Read failed");
                    }
                    System.out.println("Press enter to continue.");
                    in.nextLine();
                    break;
                //endregion

                //region Update
                case 11:
                    try{
                        staffBLL.updateStaffByID(getStaffModelFromInput());
                    }
                    catch (DALException e){
                        e.printStackTrace();
                        System.out.println("Update Failed");
                    }
                    break;
                case 12:
                    try{
                        shiftBLL.updateShiftByID(getShiftModelFromInput());
                    }
                    catch (DALException e){
                        e.printStackTrace();
                        System.out.println("Update Failed");
                    }
                    break;
                //endregion

                //region Delete
                case 13:
                    System.out.print("Please input an id: ");
                    tmpID = in.nextInt();
                    in.nextLine();
                    try{
                        staffBLL.deleteStaffByID(tmpID);
                    }
                    catch (DALException e){
                        e.printStackTrace();
                        System.out.println("Delete Failed");
                    }
                    break;
                case 14:
                    System.out.print("Please input an id: ");
                    tmpID = in.nextInt();
                    in.nextLine();
                    try{
                        shiftBLL.deleteShiftByID(tmpID);
                    }
                    catch (DALException e){
                        e.printStackTrace();
                        System.out.println("Delete Failed");
                    }
                    break;
                case 15:
                    System.out.print("Please input the staff id: ");
                    tmpID = in.nextInt();
                    System.out.print("Please input the shift id: ");
                    int tmpID2 = in.nextInt();
                    try{
                        assignmentBLL.deleteAssignmentByID(tmpID, tmpID2);
                    }
                    catch (DALException e){
                        e.printStackTrace();
                        System.out.println("Delete Failed");
                    }
                    break;
                //endregion
            }

        }while(input != 16);
    }

    private int showMenuAndGetChoice(){
        System.out.println("What would you like to do:");
        System.out.println("1. Add new staff");
        System.out.println("2. Add new shift");
        System.out.println("3. Assign worker to a shift");

        System.out.println("4. Display all staff");
        System.out.println("5. Display all shifts");
        System.out.println("6. Display all assignments");
        System.out.println("7. Display a staff member based on a staff id number");
        System.out.println("8. Display a shift based on a shift id number");
        System.out.println("9. Display all assignments for a staff");
        System.out.println("10. Display all assignments for a shift");

        System.out.println("11. Update a staff's information");
        System.out.println("12. Update a shift's information");

        System.out.println("13. Delete a staff");
        System.out.println("14. Delete a shift");
        System.out.println("15. Delete an assignment");

        System.out.println("16. Exit the program");

        System.out.print("\nPlease select a number: ");
        int input = in.nextInt();
        in.nextLine(); //we have to get rid of the new line character at the end of the line bc java
        return  input;
    }

    private StaffModel getStaffModelFromInput(){
        System.out.println();
        System.out.print("Please input a first name: ");
        String firstName = in.nextLine();
        System.out.print("Please input a last name: ");
        String lastName = in.nextLine();
        System.out.print("Please input a phone number: ");
        String phoneNumber = in.nextLine();
        System.out.print("Please input an email: ");
        String email = in.nextLine();

        return new StaffModel(firstName, lastName, phoneNumber, email);
    }

    private ShiftModel getShiftModelFromInput(){
        System.out.println();
        System.out.print("Please input a shift name: ");
        String name = in.nextLine();
        System.out.print("Please input a client name: ");
        String clientName = in.nextLine();
        System.out.print("Please input a client phone number: ");
        String clientPhoneNumber = in.nextLine();
        System.out.print("Please input an address: ");
        String address = in.nextLine();
        System.out.print("Please input an date: ");
        String date = in.nextLine();
        System.out.print("Please input an wage: ");
        Double wage = in.nextDouble();

        return new ShiftModel(name, clientName, clientPhoneNumber, address, date, wage);
    }

    private AssignmentModel getAssignmentModelFromInput(){
        System.out.println();
        System.out.print("Please input the staff id: ");
        int staffID = in.nextInt();
        System.out.print("Please input the shift id: ");
        int shiftID = in.nextInt();

        return new AssignmentModel(staffID, shiftID);
    }
}
