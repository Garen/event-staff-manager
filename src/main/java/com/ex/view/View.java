package com.ex.view;

public abstract class View {
    /***
     * This method will handle displaying information to the user
     * and should run in the Application
     * @see com.ex.app.Application
     */
    public abstract void run();
}
