package com.ex.tests;

import com.ex.bll.AssignmentBLL;
import com.ex.bll.ShiftBLL;
import com.ex.bll.StaffBLL;

import com.ex.dal.*;
import com.ex.model.AssignmentModel;
import com.ex.model.ShiftModel;
import com.ex.model.StaffModel;
import jdk.nashorn.internal.ir.annotations.Ignore;
import org.junit.jupiter.api.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ShiftEnrollmentTests {

    //region Fields
    private static int staffID;
    private static int shiftID;

    private final static String STAFF_TEST_FILE = "src/main/resources/staffTest";
    private final static String SHIFT_TEST_FILE = "src/main/resources/shiftTest";
    private final static String ASSIGNMENT_TEST_FILE = "src/main/resources/assignmentTest";

    private final static String URL = "jdbc:postgresql://event-staff-manager.cnjiicotuaxa.us-east-1.rds.amazonaws.com:5432/postgres";
    private final static String USERNAME = "trevor_serpas";
    private final static String PASSWORD = System.getenv("DB_PASSWORD");

    static StaffBLL staffBLL;
    static ShiftBLL shiftBLL;
    static AssignmentBLL assignmentBLL;
    //endregion

    /*
    @Ignore //Ignore this so we can test with databases instead
    @Test
    @BeforeAll
    public static void createBLLs_WithFile() throws DALException{
        staffBLL = new StaffBLL(new StaffFileDAL(STAFF_TEST_FILE));
        shiftBLL = new ShiftBLL(new ShiftFileDAL(SHIFT_TEST_FILE));
        assignmentBLL = new AssignmentBLL(new AssignmentFileDAL(ASSIGNMENT_TEST_FILE));
    } */

    @Test
    @BeforeAll
    public static void createBLLs_WithDataBase() throws DALException{
        staffBLL = new StaffBLL(new StaffSQLDAL(URL, USERNAME, PASSWORD));
        shiftBLL = new ShiftBLL(new ShiftSQLDAL(URL, USERNAME, PASSWORD));
        assignmentBLL = new AssignmentBLL(new AssignmentSQLDAL(URL, USERNAME, PASSWORD));
    }

    @Test
    @Order(1)
    public void createNewStaff_andGetID() throws DALException{
        staffID = staffBLL.createStaff(new StaffModel("John", "Doe", "567-8309", "jDoe@revature.com"));
    }

    @Test
    @Order(1)
    public void createNewShift_andGetID() throws DALException{
        shiftID = shiftBLL.createShift(new ShiftModel("Banquet", "Revature", "1800-rev-atur", "12345 Sesame St.", "July 4th, 8 pm", 10.0));
    }

    @Test
    @Order(2)
    public void createNewAssignment() throws DALException{
        assignmentBLL.createAssignment(new AssignmentModel(staffID, shiftID));
    }

    @Test
    @Order(2)
    public void getAllStaff() throws DALException{
        staffBLL.getAllStaff();
    }

    @Test
    @Order(2)
    public void getAllShifts() throws DALException{
        shiftBLL.getAllShifts();
    }

    @Test
    @Order(2)
    public void getStaffByID_andVerifyEmail() throws DALException{
        Assertions.assertEquals(staffBLL.getStaffByID(staffID).email, "jDoe@revature.com");
    }

    @Test
    @Order(2)
    public void getShiftByID_andVerifyClientPhoneNumber() throws DALException{
        Assertions.assertEquals(shiftBLL.getShiftByID(shiftID).clientPhoneNumber, "1800-rev-atur");
    }

    @Test
    @Order(3)
    public void updateStaffByIDToSetName_andVerifyByGetByID() throws DALException{
        staffBLL.updateStaffByID(new StaffModel(staffID, "Jane", "Doe", "567-8309", "jDoe@revature.com"));
        Assertions.assertEquals(staffBLL.getStaffByID(staffID).firstName, "Jane");
    }

    @Test
    @Order(3)
    public void updateShiftByIDToSetWage_andVerifyByGetByID() throws DALException{
        shiftBLL.updateShiftByID(new ShiftModel(shiftID, "Banquet", "Revature", "1800-rev-atur", "12345 Sesame St.", "July 4th, 8 pm", 12.0));
        Assertions.assertEquals(shiftBLL.getShiftByID(shiftID).wage, 12.0);
    }

    @Test
    @AfterAll
    public static void deleteAddedRecords() throws DALException{
        assignmentBLL.deleteAssignmentByID(staffID, shiftID);
        staffBLL.deleteStaffByID(staffID);
        shiftBLL.deleteShiftByID(shiftID);
    }
}
